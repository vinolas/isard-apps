<?php
/*
Plugin Name: Wemcor Patterns
Plugin URI:
Description: Carga patterns por defecto para Gutenberg
Author: Wemcor
Author URI: https://wemcor.com
Text Domain: wemcor-patterns
*/

/*
 * Instrucciones nombramiento de ficheros
 *
 * Guardar los templates en formato .json con formato minusculas y palabras separadas con guión medio
 * El sistema se encarga de leer dicho directorio para mostrar todos los patterns. Si se desea dar de alta un nuevo pattern tan solo hay que guardar el .json en el directorio especificado
 * El sistema se encarga de hacer un rename para mostrar nombre del template
 * Se tienen que guardar en formato .json dentro de la carpeta mu-plugins/templates
 *
 */

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Registro categoria de patrones
add_action( 'init', 'wemcor_register_block_pattern_category' );
function wemcor_register_block_pattern_category () {
	if ( ! class_exists( 'WP_Block_Patterns_Registry') ) {
		return;
	}

	register_block_pattern_category (
		'wem-templates',
		array(
			'label' => 'Encabezados'
		)
	);
}

// Registro de patrones
add_action( 'init', 'wemcor_register_block_patterns' );
function wemcor_register_block_patterns() {
	if ( ! class_exists( 'WP_Block_Patterns_Registry' ) ) {
		return;
	}

	//leemos directorio de templates para cargar todos los patterns
	require_once ABSPATH . 'wp-admin/includes/file.php';
	$files = list_files( dirname(__FILE__) . '/templates/', 1);
	foreach($files as $file) {
		// /home/customer/www/test.wemcor.es/public_html/wp-content/mu-plugins/templates/template-fashion.json
		$name = explode("/", $file);
		wemcor_register_pattern( end($name) );
	}

}

function wemcor_register_pattern( $file ) {
	$url = WPMU_PLUGIN_URL . '/templates/' . $file;
	$json = file_get_contents($url);
	$json_data = json_decode($json, true);

	//rename
	$name = str_replace('-', ' ', $file);
	$name = str_replace('.json', '', $name);

	register_block_pattern(
		'wem-gutenberg-blocks-patterns/'.$file,
		array(
			'title'    => ucfirst($name),
			'content'  => $json_data['content'],
			'categories' => [ 'wem-templates' ],
		)
	);
}
