<?php

if( is_multisite() && current_user_can( 'read' ) ) :

	$user_id = get_current_user_id();

	$title = 'Mis sitios web';

	$blogs = get_blogs_of_user( $user_id );

	?>

	<div class="wrap">
		<h1 class="wp-heading-inline">
		<?php
		echo esc_html( $title );
		?>
		</h1>

		<form id="myblogs" method="post">
			<ul class="my-sites striped">
				<?php
				//reset( $blogs );
				foreach ( $blogs as $user_blog ) {
					switch_to_blog( $user_blog->userblog_id );

					echo '<li>';
					echo '<img style="width:100%;height:auto;" alt="screenshot" src="http://s.wordpress.com/mshots/v1/'.urlencode(home_url()).'"/>';
					echo "<h3>{$user_blog->blogname}</h3>";

					$actions = "<a href='" . esc_url( home_url() ) . "'>" . __( 'Visitar web' ) . '</a>';

					if ( current_user_can( 'read' ) ) {
						$actions .= " | <a href='" . esc_url( admin_url() ) . "edit.php?post_type=page'>" . __( 'Todas las páginas' ) . '</a>';
					}

					if ( current_user_can( 'manage_sites' ) ) {
						$actions .= " | <a href='" . esc_url( admin_url() ) . "network/sites.php?page=publicarweb&id=".$user_blog->userblog_id."'>" . __( 'Publicar web' ) . '</a>';//http://test.wemcor.es/wp-admin/network/sites.php?page=publicarweb&id=1
					}

					echo "<p class='my-sites-actions'>" . $actions . '</p>';
					$pages = get_pages(['post_status'=>'publish']);
					if($pages) {
						echo '<p style="font-weight:bold;">Páginas del sitio web</p>';
						foreach($pages as $page) {
							echo '<p style="padding-left:10px;">'.$page->post_title.' <a href="'.home_url($page->post_name).'">Ver</a> | <a href="'.admin_url().'post.php?post='.$page->ID.'&action=edit">Editar</a></p>';
						}
					}
					echo '</li>';

					restore_current_blog();
				}
				?>
			</ul>
		</form>

		<?php
		// añadir botón nuevo sitio web si es teacher o superior
		if ( current_user_can( 'read' ) ) echo '<p style="margin-top: 40px;"><a href="'.admin_url().'network/site-new.php" class="button button-primary">Añadir nuevo sitio web</a></p>';//<input type="submit" name="add-site" id="add-site" class="button button-primary" value="Añadir sitio">
		?>
	</div>

<?php
endif;


