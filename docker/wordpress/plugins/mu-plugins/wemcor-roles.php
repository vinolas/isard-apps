<?php
/*
Plugin Name: Wemcor Roles Multisite
Plugin URI:
Description: Definición de roles y permisos personalizados. Anulación de roles de WordPress. Compatible con multisite
Author: Wemcor
Author URI: https://wemcor.com
Text Domain: wemcor-roles
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*
 * Roles personalizados
 *
 * Creamos dos roles que tendrán diferentes permisos
 * Rol Teacher podrá publicar sitios y crearlos
 * Rol Student podrá crear sitios pero no publicarlos
 * Ninguno de estos dos roles podrá hacer otro tipo de tareas propias de administrador como activar / editar temas y plugins
 *
 */
add_action( 'init', 'wemcor_add_custom_roles', 99 );
function wemcor_add_custom_roles() {
	//Default capabilities for Teacher
	$capabilities = [
		//pages
		'delete_others_pages' => true,
		'delete_pages' => true,
		'delete_private_pages' => true,
		'delete_publisehd_pages' => true,
		'edit_others_pages' => true,
		'edit_pages' => true,
		'edit_private_pages' => true,
		'edit_published_pages' => true,
		'publish_pages' => true,
		'read_private_pages' => true,
		//posts
		'delete_others_posts' => true,
		'delete_posts' => true,
		'delete_private_posts' => true,
		'delete_publisehd_posts' => true,
		'edit_others_posts' => true,
		'edit_posts' => true,
		'edit_private_posts' => true,
		'edit_published_posts' => true,
		'publish_posts' => true,
		'read_private_posts' => true,
		//themes
		'delete_themes' => false,
		'edit_theme_options' => false,
		'edit_themes' => false,
		'install_themes' => false,
		'switch_themes' => false,
		'update_themes' => false,
		//plugins
		'activate_plugins' => false,
		'delete_plugins' => false,
		'edit_plugins' => false,
		'install_plugins' => false,
		'update_plugins' => false,
		//users
		'add_users' => true,
		'create_users' => false,
		'delete_users' => false,
		'edit_users' => true,
		'list_users' => true,
		'promote_users' => true,
		'remove_users' => false,
		//core & others & general
		'customize' => false,
		'edit_dashboard' => false,
		'edit_files' => true,
		'export' => false,
		'import' => false,
		'manage_categories' => true,
		'manage_links' => true,
		'manage_options' => false,
		'moderate_comments' => true,
		'read' => true,
		'unfiltered_html' => true,
		'update_core' => false,
		'upload_files' => true,
	];

	//capabilities de multisite
	if ( is_multisite() ) :
		//sites (multisite)
		$capabilities['create_sites'] = false;
		$capabilities['delete_site'] = false;
		$capabilities['delete_sites'] = false;
		$capabilities['manage_network'] = true;
		$capabilities['manage_network_options'] = true;
		$capabilities['manage_network_plugins'] = false;
		$capabilities['manage_network_themes'] = false;
		$capabilities['manage_network_users'] = true;
		$capabilities['manage_sites'] = true;
		$capabilities['setup_network'] = true;
		$capabilities['upgrade_network'] = false;
	endif;

	/*
	 * Teacher
	 */
	if ( is_multisite() ) add_role( 'teacher', 'Teacher', $capabilities );

	/*
	 * Student
	 */
	//modificamos algunas de las capabilities de los argumentos para crear student
	if ( is_multisite() ) $capabilities['manage_network_users'] = false;
	if ( is_multisite() ) $capabilities['manage_sites'] = false;
	$capabilities['add_users'] = false;
	$capabilities['edit_users'] = false;
	$capabilities['list_users'] = false;
	$capabilities['promote_users'] = false;
	$capabilities['moderate_comments'] = false;
	if ( is_multisite() ) add_role( 'student', 'Student', $capabilities );

	/*
	 * Manager
	 */
	//creamos manager poniendo todas las capblities a true excepto las del array no_caps
	if ( is_multisite() ) {
		$no_caps = array(
			'manage_network_plugins',
			'manage_network_themes',
			'upgrade_network',
			'delete_themes',
			'edit_theme_options',
			'edit_themes',
			'install_themes',
			'switch_themes',
			'update_themes',
			'activate_plugins',
			'delete_plugins',
			'edit_plugins',
			'install_plugins',
			'update_plugins'
		);
		foreach( $capabilities as $key => $value ) {
			if( in_array( $key, $no_caps) ) $capabilities[$key] = false;
			else $capabilities[$key] = true;
		}
		add_role( 'manager', 'Manager', $capabilities );
	}
	$manager = get_role('manager');
}

/*
 * Eliminar Roles
 *
 * Eliminar Roles Multisite / WordPress excepto superadmin y los creados Teacher y Student
 *
 */
add_action( 'init', 'wemcor_remove_roles', 10 );
function wemcor_remove_roles() {
	if( is_multisite() ) :
		//Admin
		remove_role( 'admin' );
		//Editor
		remove_role( 'editor' );
		//Author
		remove_role( 'author' );
		//Contributor
		remove_role( 'contributor' );
		//Subscriber
		remove_role( 'subscriber' );
		/*anular cuando esté en producción*/
		//Teacher
		remove_role( 'teacher' );
		//Student
		remove_role( 'student' );
		//Manager
		remove_role( 'manager' );
	endif;
}
