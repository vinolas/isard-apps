jQuery(document).ready(() => {
    base_url = `${window.location.protocol}//${window.location.host.replace(/^nextcloud\./, 'api.')}`
    $.getJSON(`${base_url}/json`, (result) => {
        if (result.logo) {
            $("#navbar-logo img").attr('src', result.logo)
        }
    })
    $.get(`${base_url}/header/html/nextcloud`, (result) => {
        console.log(result)
        $("#navbar-logo").after(result)
        $('#dropdownMenuAppsButton').click(() => {
            $('#dropdownMenuApps').toggle()
        })
    })
})
