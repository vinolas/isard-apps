<?php
/*
Plugin Name: Wemcor Sites
Plugin URI:
Description: Muestra un dashboard al estilo Google sites con un listado de los sotes disponibles para cada usuario y las páginas creadas
Author: Wemcor
Author URI: https://wemcor.com
Text Domain: wemcor-options
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// añadir menus
add_action( 'admin_menu', 'wemcor_add_menu_mis_sitios', 10 );
function wemcor_add_menu_mis_sitios() {
	//mis sitios web
	add_menu_page(
		'Mis sitios web',
		'Mis sitios web',
		'manage_network',
		'mis-sitios',
		'wemcor_mis_sitios_callback',
		'dashicons-layout',
		2
	);

	//administrador red (admin network). Lo sacamos de admin bar y lo ponemos en menu lateral
	add_menu_page(
		'Administrador de la red',
		'Administrador de la red',
		'create-sites',
		'network',
		'',
		'dashicons-wordpress',
		1
	);

}

function wemcor_mis_sitios_callback() {
	require_once plugin_dir_path(__FILE__) . 'includes/mis-sitios.php';
}

// Borrar menú escritorio para Teachers y Students
add_action( 'admin_init', 'remove_dashboard_menu_according_role' );
function remove_dashboard_menu_according_role() {
	$user = wp_get_current_user();
	if ( in_array( 'teacher', $user->roles ) || in_array( 'student', $user->roles ) ) remove_menu_page('index.php');

	//remove_menu_page('index.php');
}


//cambiar links de adminbar (que vayan a página nueva) site.php my-site.php. Quizás mejor crear un menu nuevo que lleve solo a mis sitios (en lugar del menu de multisite). cambiar nombre de sitios a todos los sitios (solo lo ve el manager)
//al hacer click en el icono debe llevar a administrador de la red (el teacher y el manager deben tener permisos) y ver el menu de mis sitios. El student no puede acceder. manager y teacher veria usuarios, mis sitios. no ven escritorio sitios temas plugin ajustes. manager veria tambien sitios
//el manager lo ve igual que origen. No verá este menu nuevo. El teacher y el student si que lo ven y hay que añadir el link de publicar o en todo caso una opcion en formato link junto al de dashoboard y pages



//mostrar página nueva con query que lea sites del usuario logueado

//mostrar screenshot dle site

//mostrar listado de páginas

//mostrar link a dashboard que lleve a listado de páginas. Crear variable para cambiar a 'pages' or 'post'

//link a editar sitio

//redirigir al iniciar sesión a cada usuario a esta página
add_filter( 'login_redirect', 'wemcor_redirect_mis_sitios_web' );
function wemcor_redirect_mis_sitios_web() {
	return admin_url() .'/admin.php?page=mis-sitios';//http://test.wemcor.es/wp-admin/?page=mis-sitios
}



