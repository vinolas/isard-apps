<?php
/*
Plugin Name: Wemcor Functions
Plugin URI:
Description: Funciones adicionales para los plugins de wemcor
Author: Wemcor
Author URI: https://wemcor.com
Text Domain: wemcor-functions
*/

//se usa 'page'. Cambiar 'page' por 'post' si se decide usar sistema de posts en lugar de pages

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'wp_enqueue_scripts', 'wemcor_enqueue_styles' );
function wemcor_enqueue_styles() {

	wp_enqueue_style( 'wemcor-styles', WPMU_PLUGIN_URL .'/assets/style.css' );
	//wp_enqueue_style( 'wemcor-dynamic-style', WPMU_PLUGIN_URL .'/assets/style.php' );
}


// quitar títulos de las páginas para que así prevalezca el encabezamiento estilo google site
add_filter( 'the_title', 'remove_page_title' );
function remove_page_title() {
   if( is_page() ) return false;
}


// cargar título predeterminado cuando se hace click en nueva págin
add_filter( 'default_title', 'wemcor_default_title_on_page', 10, 2 );
function wemcor_default_title_on_page( $title, $post ) {
	if( 'page' === $post->post_type ) $title = 'Nueva página';

	return $title;
}

// cargar contenido predeterminado cuando se hace click en añadir nueva página
add_filter( 'default_content', 'wemcor_default_content_on_page', 10, 2 );
function wemcor_default_content_on_page( $content, $post ) {
    if( 'page' === $post->post_type ) $content = '<!-- wp:generateblocks/container {"uniqueId":"a4c6887b","containerWidth":960,"paddingTop":"180","paddingRight":"100","paddingBottom":"180","paddingLeft":"100","paddingTopTablet":"120","paddingBottomTablet":"120","paddingTopMobile":"60","paddingRightMobile":"20","paddingBottomMobile":"60","paddingLeftMobile":"20","backgroundColor":"#226e93","gradient":true,"gradientDirection":0,"gradientColorOne":"#242424","gradientColorOneOpacity":0.4,"gradientColorStopOne":0,"gradientColorTwo":"#242424","gradientColorTwoOpacity":0.4,"gradientColorStopTwo":100,"bgImage":{"id":143,"image":{"url":"http://test.wemcor.es/wp-content/uploads/2021/05/simple-header-blended-small.png","height":544,"width":725,"orientation":"landscape"}},"bgOptions":{"selector":"pseudo-element","opacity":1,"overlay":false,"position":"center center","size":"cover","repeat":"no-repeat","attachment":""},"showAdvancedTypography":true,"align":"full","isDynamic":true} -->
<!-- wp:heading {"textAlign":"center","level":1,"style":{"typography":{"fontSize":80}},"textColor":"white"} -->
<h1 class="has-text-align-center has-white-color has-text-color" style="font-size:80px">Título de la página</h1>
<!-- /wp:heading -->
<!-- /wp:generateblocks/container -->

<!-- wp:paragraph -->
<p>Empieza a escribir aquí el contenido de la página. Puedes incluir imágenes y links</p>
<!-- /wp:paragraph -->';

    return $content;
}
